# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 11:14:36 2020

@author: Angel
"""

#Variables
host = 'localhost'
port = 8051
#Se importa el módulo
import socket
 
#Creación de un objeto socket (lado cliente)
obj = socket.socket()
 
#Conexión con el servidor. Parametros: IP (puede ser del tipo 192.168.1.1 o localhost), Puerto
obj.connect((host, port))
print("Conectado al servidor")
 
#Creamos un bucle para retener la conexion
while True:
    #Instanciamos una entrada de datos para que el cliente pueda enviar mensajes
    mens = input("Mensaje al servidor: ")
    # msj_rec=obj.recv(1024)
    # print(msj_rec.decode())
    #Con el método send, enviamos el mensaje
    obj.send(mens.encode())
    
    msj_rec=obj.recv(1024)
    
    if msj_rec == 'Esperaba el comando Pizza':
        print  (msj_rec.decode())
    else:
        print  (msj_rec.decode())
    
        mensn = input("")
        obj.send(mensn.encode())
    
        msj_rec=obj.recv(1024)    
        print (msj_rec.decode())
        # print("Revista tu facebook, la información ha sido publicada")
    
    
#Cerramos la instancia del objeto servidor
obj.close()

#Imprimimos la palabra Adios para cuando se cierre la conexion
print("Conexión cerrada")