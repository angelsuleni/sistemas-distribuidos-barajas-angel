# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 15:01:42 2020

@author: Angel
"""
import requests
from bs4 import BeautifulSoup

r = requests.get('https://www.sindelantal.mx/delivery/nezahualcoyotl-mex/pizzas-y-parrilla-charly-metropolitana-2da-seccion/0a21d930-0e0e-44e5-bc6b-2140c3f29ae3')
soup = BeautifulSoup(r.content, 'lxml')

resultado = soup.find("ul", {"class": "restaurant-menu-group__container"})
# print(resultado.prettify())
Pizza=resultado.find_all('h3', class_='dish-card__info')

for Pizza in Pizza:
    Precio = Pizza.find('span', class_='dish-card__price')
    Tipo = Pizza.find('span', class_='dish-card__description')

    if None in (Tipo, Precio):
        continue
    print(Tipo.text.strip())
    print(Precio.text.strip())

    print()