# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 19:02:53 2020

@author: Angel
"""
import requests
from bs4 import BeautifulSoup

URL = 'https://www.monster.com/jobs/search/?q=Software-Developer&where=Australia'
pagina = requests.get(URL)

soup = BeautifulSoup(pagina.content, 'html.parser')
#resultado = soup.find(id='ResultsContainer')
#print(resultado.prettify())
tabajos = soup.find_all('section', class_='card-content')


for tabajos in tabajos:
    titulo = tabajos.find('h2', class_='title')
    compania = tabajos.find('div', class_='company')
    localizacion = tabajos.find('div', class_='location')
    if None in (titulo, compania, localizacion):
        continue
    print(titulo.text.strip())
    print(compania.text.strip())
    print(localizacion.text.strip())
    print()
    
    