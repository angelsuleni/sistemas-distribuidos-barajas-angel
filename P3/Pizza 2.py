# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 19:02:53 2020

@author: Angel
"""
import requests
from bs4 import BeautifulSoup

r = requests.get('https://www.sindelantal.mx/delivery/cuauhtemoc-df/pizzas-delezza-centro/a0ee6054-7257-49ae-a036-1e0cb3967d34/')
soup = BeautifulSoup(r.content, 'lxml')

resultado = soup.find("ul", {"class": "restaurant-menu-group__container"})
# print(resultado.prettify())
Pizza=resultado.find_all('h3', class_='dish-card__info')

for Pizza in Pizza:
    Precio = Pizza.find('span', class_='dish-card__price')
    Tipo = Pizza.find('span', class_='dish-card__description')

    if None in (Tipo, Precio):
        continue
    print(Tipo.text.strip())
    print(Precio.text.strip())

    print()