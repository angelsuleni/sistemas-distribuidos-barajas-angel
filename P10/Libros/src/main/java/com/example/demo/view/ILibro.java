package com.example.demo.view;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Libro;
@Repository
public interface ILibro extends CrudRepository <Libro, Integer>{

}
