package com.example.demo.interfaceservice;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.Libro;

public interface ILibroService {
	public List<Libro>listar();
	public Optional<Libro>listarId(int ID);
	public int save(Libro l);
	public void delete (int id);
	
}
