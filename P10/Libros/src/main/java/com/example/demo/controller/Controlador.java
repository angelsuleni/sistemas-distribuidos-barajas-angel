package com.example.demo.controller;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.interfaceservice.ILibroService;
import com.example.demo.model.Libro;

@Controller
@RequestMapping
public class Controlador {
	@Autowired
	private ILibroService service;
	
	@GetMapping("/listar")
	public String listar(Model model) {
		List<Libro>libro=service.listar();
		model.addAttribute("libro",libro);
		return "index";
	}
	
	@GetMapping("/new")
	public String agregar(Model model) {
		
		model.addAttribute("libro",new Libro());
		return "form";
	}
	
	@PostMapping("/save")
	public String save(@Valid Libro l, Model model) {
		
		service.save(l);
		return "redirect:/listar";
	}
	
	@GetMapping("/editar/{id}")
	public String editar(@PathVariable int id, Model model) {
		Optional<Libro>libro=service.listarId(id);
		model.addAttribute("libro", libro);
		return "form";
	}
	@GetMapping("/eliminar/{id}")
	public String delete(Model model, @PathVariable int id) {
		service.delete(id);
		return "redirect:/listar";
		
	}

}
