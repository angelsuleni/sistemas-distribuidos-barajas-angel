<?php
	// Cabeceras
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	// Decodificación del json
	$data = json_decode(file_get_contents("php://input"));
	$met = $_SERVER['REQUEST_METHOD'];
	$uri = $_SERVER['REQUEST_URI'];

	// Función de conexión
	function Conectar(){
		// Datos de conexión
		$servername = "mysql1004.mochahost.com";
		$username = "nsierrar_sd2020";
		$password = "n513rr4r5d";
		$dbname = "nsierrar_sd2020";
		// Crear conexión
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Revisar conexión fallida
		if ($conn->connect_error) {die("Fallo en conexión: " . $conn->connect_error);	}
		// Conexión ejecutada correctamente
		else{	echo "Se ha conectado correctamente con el servidor <br> ";}
		return $conn;
	}
	// Condiciones de elección de CRUD
	// Condición método POST
	if ($met == 'POST'){
			if(!empty($data->id) && !empty($data->name) && !empty($data->description) && !empty($data->price)){
				http_response_code(201);
				$conn = Conectar();
				$sql = "INSERT INTO books (id, name, description, price, eliminado) VALUES  ('".$data->id."','".$data->name."','".$data->description."',".$data->price.",0)";
				if ($conn->query($sql) === TRUE) { 	$message="Nueva información guardada exitosamente"; }
				else { $message="Error: " . $sql . ", " . $conn->error; }
				$conn->close();
				echo json_encode(array("Metodo :" => $met, "Mensaje : " => $message, "Datos recibidos : " => array("ID :" => $data->id, "Nombre : " => $data->name, "Dexcripcion : " => $data->description, "Precio : " => $data->price, "Eliminados : ",0)));
			}
			else{
				http_response_code(503);
				echo json_encode(array("Mensaje : " => "No se han recibido datos"));
			}
		}
		// Condición método PUT
		elseif ($met =='PUT'){
			if(!empty($data->id)){
				http_response_code(201);
				$conn = Conectar();
				$sql = "UPDATE books SET ";
				if(!empty($data->name)){$sql = $sql." name='".$data->name."', ";}
				if(!empty($data->description)){$sql = $sql."description='".$data->description."', ";}
				if(!empty($data->price)){$sql = $sql."price='".$data->price."', ";}
				if(!empty($data->eliminado)){$sql = $sql." eliminado'".$data->eliminado."', ";}
				if(!empty($data->id)){$sql = $sql."id='".$data->id."' ";}
				$sql = $sql." WHERE id='".$data->id."'";
				if ($conn->query($sql) === TRUE) {$message="Nueva información guardada exitosamente";	}
				else {$message="Error : " . $sql . ", " . $conn->error;}
				$conn->close();
				echo json_encode(array("Método:" => $met, "Mensaje : " => $message, "Datos recividos : " => array("id : " => $data->id)));
			}
			else{
				http_response_code(503);
				echo json_encode(array("Mensaje" => "No se recinieron datos"));
			}

		}

		// Condición método DELETE
		elseif ($met == 'DELETE'){
			if(!empty($data->id)){
				http_response_code(201);
				$conn = Conectar();
				$sql = $sql = "UPDATE books SET eliminado=eliminado+1 WHERE id='".$data->id."'";
				if ($conn->query($sql) === TRUE) {$message="Nueva información guardada exitosamente";	}
				else { $message="Error : " . $sql . ", " . $conn->error; }
				$conn->close();
				echo json_encode(array("Método : " => $met, "Mensaje : " => $message, "Datos recibidos : " => array( "id : " => $data->id)));
			}
			else{
				http_response_code(503);
				echo json_encode(array("Mensaje : " => "No se recinieron datos"));
			}
		}

		// Condición método GET
		elseif($met == 'GET'){
			$conn = Conectar();
			$sql = "SELECT * FROM books";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {$json[] = array( "id" => $row["id"], "nombre" => $row["name"], 	"descripcion" => $row["description"], "precio" => $row["price"], "eliminado" => $row["eliminado"]);}
				echo json_encode($json);
			}
			else { echo "0 encontrados"; }
			$conn->close();
		}

		// Condición OTRO
		else{
			http_response_code(503);
			echo json_encode(array("Mensaje" => "No se recinieron datos"));
		}
?>
