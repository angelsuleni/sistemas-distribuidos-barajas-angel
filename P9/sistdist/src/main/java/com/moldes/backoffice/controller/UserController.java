package com.moldes.backoffice.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class UserController {
	
	@GetMapping("/getUserController")
	public String getUserController() {
		return "Hola, soy el WS";
	}
}