package com.example.sistdist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistdistApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistdistApplication.class, args);
	}

}
