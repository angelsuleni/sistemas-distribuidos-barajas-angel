package com.example.demo.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.modelo.Traductor;

@Repository
public interface Itraductor extends CrudRepository <Traductor, Integer>{

}
