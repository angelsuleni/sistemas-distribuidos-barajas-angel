package com.example.demo.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.demo.modelo.Traductor;

public interface ItraductorService {
	public List<Traductor>listar();
	public Optional<Traductor>listarId(int id);
	public int save(Traductor t);
	void delete (int id);
	
}
