package com.example.demo.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "numero")
public class Traductor {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int numero;
		private String espanol;
		private String hindi;
		
		public Traductor() {
			super();
		}

		public int getNumero() {
			return numero;
		}

		public void setNumero(int numero) {
			this.numero = numero;
		}

		public String getEspanol() {
			return espanol;
		}

		public void setEspanol(String espanol) {
			this.espanol = espanol;
		}

		public String getHindi() {
			return hindi;
		}

		public void setHindi(String hindi) {
			this.hindi = hindi;
		}

		public Traductor(int numero, String espanol, String hindi) {
			super();
			this.numero = numero;
			this.espanol = espanol;
			this.hindi = hindi;
		}
		
		
}
