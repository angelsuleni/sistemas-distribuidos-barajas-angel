package com.example.demo.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.interfaceService.ItraductorService;
import com.example.demo.modelo.Traductor;

@Controller
@RequestMapping
public class Controlador {
	
	@Autowired
	private ItraductorService service;
	
	@GetMapping("/listar")
	public String Listar(Model model) {
		List<Traductor>traductores=service.listar();
		model.addAttribute("traductores", traductores);
		return "index";
	}
}
