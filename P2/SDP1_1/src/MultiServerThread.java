import java.net.*;
import java.io.*;
import java.util.Date;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	    while((lineIn = entrada.readLine()) != null){
                
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
			      break;
			   }else{
               int[ ] gatos ={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
              
               int ngatos=0;
               
               for (int x=0; x<lineIn.length(); x++){
                   
                    String op="#";
                    
                    if (lineIn.charAt(x)==op.charAt(0)){
                       gatos[ngatos]=x;
                       ngatos=ngatos+1;
                    }
               }
               
               
               String m="m";  
               String a="a";  String i="i";
               String y="y";  String n="n";  String v="v";
               
               String l="l";
               String e="e";
               
               String Salida;
               if (lineIn.charAt(1)==m.charAt(0) && lineIn.charAt(2)==a.charAt(0) && lineIn.charAt(3)==y.charAt(0)){
                   
                   Salida="#R-may#"+String.valueOf(lineIn.charAt(5))+"#"+lineIn.substring(7).toUpperCase();
                                              
                   escritor.println(Salida);
                   escritor.flush();
                   
               }
               else if (lineIn.charAt(1)==m.charAt(0) && lineIn.charAt(2)==i.charAt(0) && lineIn.charAt(3)==n.charAt(0)){
                   
                   Salida="#R-min#"+String.valueOf(lineIn.charAt(5))+"#"+lineIn.substring(7).toLowerCase();
                   
                   escritor.println(Salida);
                   escritor.flush();
               }
               else if (lineIn.charAt(1)==i.charAt(0) && lineIn.charAt(2)==n.charAt(0) && lineIn.charAt(3)==v.charAt(0)){
                   Salida="#R-inv#"+String.valueOf(lineIn.charAt(5))+"#";
                   for (int contar=0; contar < Integer.parseInt(String.valueOf(lineIn.charAt(5))); contar++){
                       String voltear=lineIn.substring(gatos[2+contar]+1, gatos[3+contar]);
                       String invertida = "";
                    
                    for (int indice = voltear.length() - 1; indice >= 0; indice--) {
			
			invertida += voltear.charAt(indice);
                    }
                       Salida=Salida+invertida+"#";
                       
                   }
                   
                   escritor.println(Salida);
                   escritor.flush();
               }
               else if (lineIn.charAt(1)==l.charAt(0) && lineIn.charAt(2)==e.charAt(0) && lineIn.charAt(3)==n.charAt(0)){
                   
                   Salida="#R-len#"+String.valueOf(lineIn.charAt(5))+"#";
                   for (int contar=0; contar < Integer.parseInt(String.valueOf(lineIn.charAt(5))); contar++){
                       String voltear=lineIn.substring(gatos[2+contar], gatos[3+contar]);
                       int tamano=voltear.length();
                       Salida=Salida+Integer.toString(tamano-1)+"#";
                       
                   }
                   
                   escritor.println(Salida);
                   escritor.flush();
               }
               else if (lineIn.charAt(1)==a.charAt(0) && lineIn.charAt(2)==l.charAt(0) && lineIn.charAt(3)==e.charAt(0) && lineIn.charAt(4)==a.charAt(0)){
                   
                   Salida="#R-alea#"+String.valueOf(lineIn.charAt(6))+"#";
                   for (int contar=0; contar < Integer.parseInt(String.valueOf(lineIn.charAt(6))); contar++){
                       String alea=lineIn.substring(gatos[2+contar]+1, gatos[3+contar]);
                       int valor=Integer.parseInt(alea);
                       int aleato=(int) Math.floor(Math.random()*valor+1);
                       Salida=Salida+Integer.toString(aleato)+"#";
                       
                   }
                   
                   escritor.println(Salida);
                   escritor.flush();
                   
               }
               else{
                   escritor.println("Opcion invaida");
                   escritor.flush();
               }
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
